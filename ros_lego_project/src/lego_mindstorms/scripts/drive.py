#!/usr/bin/env python

import rospy

from geometry_msgs.msg import Twist
from std_msgs.msg import UInt8
from std_msgs.msg import Bool

# Globale Variablen fuer Sensorendaten
left_color_data = 0
right_color_data = 0
touch_data = False
touch_data_back = False

# Variablen fuer Richtungssuche von Linie zum Ball
first_time_white = True
first_time_blue = True
after_second_blue = False
start = True

drive_direction_is_left = True
drive_direction_is_right = False

# verschiedene Modi im Fahrt Verlauf
normal_modus = True
search_direction_to_ball_modus = False
search_ball_modus = False
check_for_blue_cross_modus = False
drive_back_to_cross_modus = False
drive_back_to_line_modus = False

#Farben
non_color = 0
black = 1
blue = 2
white = 6

def listener():

    global normal_modus
    global check_for_blue_cross_modus
    global search_direction_to_ball_modus
    global search_ball_modus
    global drive_back_to_cross_modus
    global touch_data_back
    global drive_back_to_line_modus

    global start
    global drive_direction_is_left
    global drive_direction_is_right
    global first_time_blue
    global after_second_blue
    global first_time_white



    # zum Zaehlen, wie oft die Farbe Blau zurueck gegeben wurde
    blue_counter = 0


    rospy.init_node("lisa_controller", anonymous=True)
    # Subscriber fuer Farbsensoren und Touch-Sensor
    rospy.Subscriber("/color_left", UInt8, callback_left)
    rospy.Subscriber("/color_right", UInt8, callback_right)
    rospy.Subscriber("/touch", Bool, callback_touch)

    rate = rospy.Rate(10)

    drive_right = False
    drive_left = False

    while True:

        #Einfacher Modus, wo der Roboter nur der Linie folgen muss
        if normal_modus == True:


            # nach links korrigieren, falls nur der linke Farbsensor Schwarz zurueckgibt
            if left_color_data == black:
                drive_left = True
                driveLeft()

            # nach rechts korrigieren, falls nur der rechte Farbsensor Schwarz zurueckgibt
            elif right_color_data == black:
                driveRight(0.1, -0.2)

            elif right_color_data == black and left_color_data == black:
                if drive_left == True:
                    driveLeft()
                else:
                    driveRight(0.1, -0.2)

            # stehen bleiben, wenn Farbsensoren unbestimmte Farbe zurueckgeben
            elif left_color_data == non_color and right_color_data == non_color:
                driveOnLine(0)
                drive_left = False

            elif left_color_data == blue and right_color_data != blue:
                driveLeft()
                drive_left = False

            elif right_color_data == blue and left_color_data != blue:
                driveRight(0.1, -0.4)
                drive_left = False

            # Pruefen ob es sich um das blaue Kreuz handelt oder nicht
            elif left_color_data == blue and right_color_data == blue:
                check_for_blue_cross_modus = True
                normal_modus = False
                drive_left = False
                print("Kreuz suche")

            # in jedem anderen Fall soll der Roboter gerade aus fahren
            else:
                driveOnLine(0.2)
                drive_left = False

        # Modus indem der Roboter prueft ob es sich um das blaue Kreuz handelt
        elif check_for_blue_cross_modus == True:


            # Zaehlt den blue_counter hoch, wenn beide Farbsensoren Blau erfassen
            if left_color_data == blue or right_color_data == blue:
                driveOnLine(0.2)
                blue_counter = blue_counter + 1
                print(blue_counter)

            # Wenn einer der Sensoren kein Blau mehr hat ...
            elif left_color_data != blue or right_color_data != blue:
                # ... der blue_counter etwas zwischen 20 und 25 gezaehlt hat
                # wurde das blaue blaue Kreuz gefunden ...
                if 8 <= blue_counter:
                    blue_counter = 0
                    search_direction_to_ball_modus = True
                    check_for_blue_cross_modus = False
                    print ("Richtung von Ball suche")

                # ... ansonsten springt der Roboter zurueck in den normalen Modus
                else:
                    blue_counter = 0
                    normal_modus = True
                    check_for_blue_cross_modus = False
                    print ("Normal")



        # Modus in dem der Roboter versucht die Fahrrichtung zum Ball zu ermitteln
        elif search_direction_to_ball_modus == True:

            if left_color_data == blue and right_color_data == blue:
                # Modusbeginn wird abgeschlossen
                if start:
                    driveOnLine(-0.2)
                    start = False
                # Soll solange zurueck fahren bis kein Blau mehr
                elif first_time_blue:
                    driveOnLine(-0.2)
                elif drive_direction_is_left:
                    after_second_blue = True
                    driveLeft()
                else:
                    driveRight(-0.1, -0.2)

            # Wird auf anhieb Schwarz auf links getroffen wurde die Strecke gefunden
            elif left_color_data == black:
                drive_direction_is_left = True
                search_ball_modus = True
                search_direction_to_ball_modus = False
            elif right_color_data == black:
                print ("Schwarz rechts")
                drive_direction_is_left = False
                search_ball_modus = True
                search_direction_to_ball_modus = False
            else:
                # Zurueck fahren bis Blau getroffen wird
                if start:
                    driveOnLine(-0.2)
                # Vorwaerts fahren bis Blau getroffen wird
                elif first_time_blue:
                    driveOnLine(0.2)
                    first_time_blue = False
                elif after_second_blue:
                    if left_color_data == white and right_color_data == white:
                        drive_direction_is_left = False
                        drive_direction_is_right = True
                        driveRight(-0.1, -0.2)
                        after_second_blue = False
                    else:
                        driveLeft()
                else:
                    if drive_direction_is_left:
                        print ("lInks")
                        driveOnLine(0.2)
                    else:
                        driveRight(-0.1, -0.2)





        # Wenn der Roboter die Richtung gefunden hat in der sich der Ball befindet wechselt er in den Ball modus
        elif search_ball_modus == True:


            if touch_data_back == False and touch_data == False:
                if left_color_data == black:
                    driveLeft()
                elif right_color_data == black:
                    driveRight(0.2, -0.4)
                else:
                    driveOnLine(0.2)
            elif touch_data == True:
                driveOnLine(-0.4)
                touch_data_back = True

            elif touch_data_back == True:
                # Wechsel in andern Modus wenn Ball gefunden wurde
                search_ball_modus = False
                drive_back_to_cross_modus = True
                print ("zurueck zum Kreuz")

        # Zurueck fahren bis zum Blauen Kreuz und dann Modus Wechsel
        elif drive_back_to_cross_modus == True:

            if left_color_data == blue and right_color_data == blue:
                drive_back_to_cross_modus = False
                drive_back_to_line_modus = True
                print ("Zurueck zur Linie")


            else:
                driveOnLine(-0.2)
        # Roboter soll von Kreuz runter fahren und
        # sich dann in die gemerkte Richtung drehen bis er Schwarz trifft und soll dann wieder Normal fahren
        elif drive_back_to_line_modus == True:

            if left_color_data == blue and right_color_data == blue:
                driveOnLine(-0.2)
            elif left_color_data == black:
                drive_back_to_line_modus = False
                normal_modus = True
                print ("Normal")
            elif right_color_data == black:
                drive_back_to_line_modus = False
                normal_modus = True
                print ("Normal")
            else:
                if drive_direction_is_left and drive_direction_is_right:
                    driveRight(0.2, 0.4)
                else:
                    driveRight(0.2, -0.4)

        rate.sleep()
    # Ueberpruefe ob es sich bei dem blauen Treffer um das blaue Kreuz handel
    rospy.spin()

# Callbacks, welche Daten von Sensoren in globalen Variabelen speichern

def callback_left(data):

    global left_color_data
    left_color_data = data.data

    print("links", left_color_data)


def callback_right(data):

    global right_color_data
    right_color_data = data.data
    print("rechts", right_color_data)

def callback_touch(data):
    global touch_data
    touch_data = data.data
    #print ("Touch", touch_data)


# Methoden zum Fahren
# nur gerade aus fahren  bzw rueckwaerts mit gewissen Tempo
def driveOnLine(x):

    publisher = rospy.Publisher('/lisa/diffDrv/cmd_vel', Twist, queue_size=10)
    move_message = Twist()
    move_message.linear.x = x

    
    publisher.publish(move_message)
    #print("Drive")

# Je nach angabe rechts oder links , war urspruenglich nur zum rechts fahren gedacht
def driveRight(x,z):

    publisher = rospy.Publisher('/lisa/diffDrv/cmd_vel', Twist, queue_size=10)
    move_message = Twist()
    move_message.linear.x = x

    move_message.angular.z = z

    publisher.publish(move_message)
    #print("drive right")

# nur links fahren
def driveLeft():
    publisher = rospy.Publisher('/lisa/diffDrv/cmd_vel', Twist, queue_size=10)
    move_message = Twist()
    move_message.linear.x = 0.1

    move_message.angular.z = 0.2

    publisher.publish(move_message)



if __name__ == "__main__":
    listener()
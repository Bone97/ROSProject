#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/lisa/Schreibtisch/ROSProjekt/ROSProject/ros_lego_project/devel/.private/lego_mindstorms:$CMAKE_PREFIX_PATH"
export PWD="/home/lisa/Schreibtisch/ROSProjekt/ROSProject/ros_lego_project/build/lego_mindstorms"
export ROSLISP_PACKAGE_DIRECTORIES="/home/lisa/Schreibtisch/ROSProjekt/ROSProject/ros_lego_project/devel/.private/lego_mindstorms/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/lisa/Schreibtisch/ROSProjekt/ROSProject/ros_lego_project/src/lego_mindstorms:$ROS_PACKAGE_PATH"